
;(function($) {

   'use strict'
  $(".scroll-link").on("click", function(e) {
    e.preventDefault();
    var $target = "#" + $(this).data("target");
    if( window.location.pathname !== "/" ) {
      window.location.href = "/" + $target;
    } else {
      $("html, body").animate({
        scrollTop: $($target).offset().top -100
      }, 500);
    }
  });
})(jQuery);